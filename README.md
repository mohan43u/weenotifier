### WeeNotifier

Weechat python plugin which is capable of communicating with [gotify](https://gotify.net) or [ntfy.sh](https://ntfy.sh). This can be a great suppliment for [weechat-android](https://github.com/ubergeek42/weechat-android). We need to keep [weechat-android](https://github.com/ubergeek42/weechat-android) running in order to get notifications from weechat. This is becoming impossible to achieve in modern android because of battery optimizations.

### Setup

#### In Weechat

* Copy __src/weenotifier.py__ from this repository to your weechat's python directory (eg: `~/.weechat/python/`)
* Optionaly enable autoloading (eg: `cd ~/.weechat/python/autoload/; ln -s ../weenotifier.py`)
* Start weechat and set __plugins.var.python.weenotifier.url__ to either __https://ntfy.sh/youruniquetopic__ or __https://selfhostedgotify.yourdomain.tld/message__
* For gotify you need to set __plugins.var.python.weenotifier.token__ to the app token from retriving from gotify server
* Restart weechat

#### In Your Mobile

* Download [gotify](https://gotify.net) or [ntfy.sh](https://ntfy.sh)
* For gotify Login with your account credentials
* wait for messages from weechat to appear
